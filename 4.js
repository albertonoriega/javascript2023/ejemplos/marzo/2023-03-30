// variable que apunta al boton de enviar
let boton = document.querySelector('#boton');
// variable que apunte al div con id caja
let caja = document.querySelector('#caja');

//Cuando pulsas el boton
boton.addEventListener('click', function (evento) {

    //Guardamos el valor del tamaño introducido en el formulario y le concatenamos string "px"
    let ancho = document.querySelector('#tamano').value + "px";
    //Guardamos el valor del grosor introducido en el formulario y le concatenamos string "px"
    let grosor = document.querySelector('#grosor').value + "px";
    //Cambiamos el ancho de la caja
    caja.style.width = ancho;
    caja.style.height = ancho;
    //Cambiamos el borde de la caja
    caja.style.border = `${grosor} solid black`;
    // vaciamos el contenido del div
    caja.innerHTML = "";
});