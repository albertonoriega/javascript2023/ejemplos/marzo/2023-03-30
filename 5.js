// variable que apunta al boton enviar
let boton = document.querySelector('#enviar');
// variable que apunta al div con id caja
let caja = document.querySelector('#caja');

// Evento que sucede cuando pulsas el boton enviar
boton.addEventListener('click', function () {
    // Guardamos el valor del input direccion1
    let direccion1 = document.querySelector('#direccion1').value;
    // Guardamos el valor del input direccion2
    let direccion2 = document.querySelector('#direccion2').value;
    // Comprobamos si los dos datos introducidos son iguales 
    // cambiamos el contenido de la caja en función de sin son iguales o no
    if (direccion1.toLowerCase() == direccion2.toLowerCase()) {
        caja.innerHTML = "Correcto";
        caja.style.color = "green";
        caja.style.backgroundColor = "#ccc";
        caja.style.width = "min-content";
    } else {
        caja.innerHTML = "Incorrecto";
        caja.style.color = "red";
        caja.style.backgroundColor = "#ccc";
        caja.style.width = "min-content";
    }
});