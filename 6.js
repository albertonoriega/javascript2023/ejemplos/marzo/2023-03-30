// variable que apunta al boton convertir
let boton = document.querySelector('#convertir');
// variable que apunta al div que muestra los resultados
let caja = document.querySelector('.resultado');

// Evento cuando pulsamos el boton convertir
boton.addEventListener('click', function () {

    //Limpiamos la caja de resultados
    caja.innerHTML = "";

    //Variable que guarda el numero de bytes introducidos por el usuario
    let bytes = document.querySelector('#bytes').value;

    // Si cumple las condiciones:
    if (bytes > 0 && bytes < 2000000000) {
        //creamos un array llamando a la funcion 
        let salida = [];
        salida = conversionBytes(bytes);

        // Cambiamos el contenido de la caja
        for (let i = 0; i < salida.length; i++) {
            caja.innerHTML += `${salida[i]} <br>`;
        }
    } else {
        // si los bytes introducidos no son correctos
        caja.innerHTML = "Valor introducido no válido"
    }
});

/**
 * Le pasas por parametro el numero de bytes y calcula los Kbs, MBs y GBs
 * @param {*} dato numeros de bytes
 * @returns Array con los KB, MB y GB
 */
function conversionBytes(dato) {
    let resultado = [];
    // Calculamos cada valor y los guardamos en el array resultado
    let kilobytes = (dato / 1024).toFixed(3) + " Kbs";
    resultado.push(kilobytes);
    let megabytes = (dato / (1024 ** 2)).toFixed(3) + " Mbs";
    resultado.push(megabytes);
    let gigabytes = (dato / (1024 ** 3)).toFixed(3) + " Gbs";
    resultado.push(gigabytes);
    // Devolvemos el array con todos los resultados
    return resultado;
}
